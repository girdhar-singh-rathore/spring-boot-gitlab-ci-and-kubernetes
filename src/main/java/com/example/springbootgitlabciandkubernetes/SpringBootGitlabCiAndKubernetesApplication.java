package com.example.springbootgitlabciandkubernetes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGitlabCiAndKubernetesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootGitlabCiAndKubernetesApplication.class, args);
    }

}
